## Simulating Evacuation of Heterogenous Pedestrians Using Fuzzy Logic

Most models designed to simulate the dynamic behavior of pedestrians are based on the assumption that human 
decision making can be described by accurate values. The study, on which this paper 
is based (Fuzzy Logic-Based Model That Incorporates Personality Traits for Heterogeneous 
Pedestrians, Zhuxin Xue et al.), proposes a new model for pedestrians that incorporates 
fuzzy logic theory into a multi-agent system to address cognitive behavior that introduces 
uncertainty and imprecision during decision-making. In addition to implementing personality 
model, our goals were to also implement fuzzy logic-based movement model (Modeling of Crowd 
Evacuation With Assailants via a Fuzzy Logic Approach, Min Zhou et al.), as the original paper
assumes perfect movement, and, if we had time, to implement assailant and observe his impact 
on pedestrians. We have successfully implemented a hybrid method for simulating a heterogeneous 
set of pedestrians, which combines motion simulation and personality influence. To back this up,
we ran similar tests as other such studies and got comparable results. Unfortunately, we ran out
of time to include an assailant, so this can perhaps be topic of future research. We have also 
noticed the disparity between our results for certain personality traits and what would be
expected from the trait definition.


### Setting up the simulation
The simulation is based on Unity environment v2019.4.14.  
As all simulations have to be started from ***Loader*** scene, we have prepared a 
quality of life shortcut to enable Loader being run every time the application
is ran from Unity Editor, regardless of the currently selected scene. It can be enabled from
Unity Editor:
>File -> Scene Autoload -> Select Master Scene... -> Scenes/Loader.unity

### Running the simulation
All simulations have to be started from ***Loader*** scene, which can be bypassed
by following the instructions above. The specific simulation can be chosen in the
***Loader script*** contained within ***Loader Controller***. Simulation options
are as following: *Testing Grounds*, *Square* and *Obstacle*. They are intented to be run
directly from Unity Editor environment, but can also be compiled in which case the
parameters will no longer be set for each test.

Each simulation is defined as its own scene, composed of a controller, one or more
spawners, goal and obstacles. The parameters of the simulations are set within
**Controller** and **Spawner** objects containing **MainController** and **Spawner** 
scripts respectively, which are thoroughly described in **Controller** and **Spawner Parameters** section. 
Both allow to define whether the simulation uses personality
traits, the percentage of agents that have set personality traits and
the minimum and maximum personality values they use. Whether the simulations
use parameters from **Controller** or **Spawner** is defined with *Override
Spawners* parameter in **Spawner**. **Controller** also defines the time step
of the simulation, whether the simulation uses 3D models and logs the output data.
The center of agents spawn area is at the position of their Spawner object.

The UI of the simulations has the following control buttons and displays:
- *Play/Pause* button,
- *R* button, which resets the simulation,
- *Exit* button which closes the application,
- *Filter* dropdown menu, which colors 2D agents on Red - Green scale, depending
on their individual personality trait values,
- *Time* which displays passed time in the respect of the simulation,
- *Agents* which displays current number of agents in the scene.

### Controller Parameters
| Parameter name   |      Description      |
|-------------|:----------|
| Timestep |  Controls the timestep used in simulation. |
| Use Personality  |    Controls whether the simulation uses personality traits.   |
| Write Outputs | Controls whether simulation logs the results. The results are logged in *Documents/Pedestrians* folder on *Windows*.|
| Output File Name | Sets the file name for logs. |
| Preferred Velocity | Sets preferred velocity used by pedestrians, only used if *Use Personality* is set to False. |
| Comment | The line added at the end of log files. |
| Override Spawners | If set to True, the *Preferred Velocity*, *Personality Percentage*, and *[Min/Max] [Personality Trait Name]* parameters in *Spawners* will be overwritten by this controller. |
| Personality Percentage | Controls the number of agents that will have personality set in regards to *[Min/Max] [Personality Trait Name]* parameters. |
| [Min/Max] [Personality Trait Name] | A set of parameters which control the range of personality trait values that are assigned to agents at random. |

### Spawner Parameters
| Parameter name   |      Description      |
|-------------|:----------|
| N Pedestrians | Controls the number of pedestrians spawned |
| Spawn Size | Size of the area that the agents will be spawned across. |
| Spawn Noise | The random factor in the spawn location of agents, if set to 0 they will always spawn in the same location.
| Goal |  Object that is used as a goal for the agents spawned by the Spawner. |
| Starting Rotation | Starting rotation of agents, 0 represents right on 2D plane. |
| Preferred Velocity | Sets preferred velocity used by pedestrians, only used if *Use Personality* is set to False. |
| Use Personality  |    Controls whether the simulation uses personality traits. |
| Personality Percentage | Controls the number of agents that will have personality set in regards to *[Min/Max] [Personality Trait Name]* parameters. |
| [Min/Max] [Personality Trait Name] | A set of parameters which control the range of personality trait values that are assigned to agents at random. |

### Making changes to the simulations
When adding new obstacles, their tag must be set to "Wall", similarly
for new spawners their tag has to be set to "Spawner". The prefabs
for Spawner and Controller are located in *Prefabs* folder. All 
UI elements are optional. Prefabs for 2D and 3D models are located in
*Resources* folder. Each spawner has to have a *Goal* object set.
When adding new scenes for simulations, they also have to be defined in
Loader script, so they can be picked when the simulation is loaded.


