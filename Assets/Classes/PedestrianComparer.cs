﻿using System.Collections.Generic;

public class PedestrianComparer  : IComparer<Pedestrian> {
    public int Compare(Pedestrian x, Pedestrian y) {
        if (ReferenceEquals(x, y)) return 0;
        if (ReferenceEquals(null, y)) return 1;
        if (ReferenceEquals(null, x)) return 1;
        if (ReferenceEquals(null, x.controller)) return 1;
        if (ReferenceEquals(null, y.controller)) return 1;
        return x.distance.CompareTo(y.distance);
    }
}