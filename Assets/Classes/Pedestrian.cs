﻿
using System;
using System.Collections.Generic;

public class Pedestrian {
    public PedestrianMovementController controller;
    public float distance = 0;
    public float pedestrianAngle = 0;

    public void SetPedestrian(PedestrianMovementController pedestrian, float distance, float pedestrianAngle) {
        this.controller = pedestrian;
        this.distance = distance;
        this.pedestrianAngle = pedestrianAngle;
    }

    public static int CompareByDistance(Pedestrian pedestrian1, Pedestrian pedestrian2) {
        return pedestrian1.distance.CompareTo(pedestrian2.distance);
    }

    // public int CompareTo(object obj) {
    //     Pedestrian otherTemperature = obj as Temperature;
    //     return pedestrian1.distance.CompareTo(pedestrian2.distance);
    // }
    
}