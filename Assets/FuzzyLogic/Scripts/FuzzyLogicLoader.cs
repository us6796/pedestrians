﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;

public static class FuzzyLogicLoader {

    private const char SEPARATOR = ';';
    private const string INPUT = "I";
    private const string OUTPUT = "O";
    public static void Load() {
        Debug.Log("Up and running");
        string dataAsJson;
        string filePathDefinitions = Path.Combine(Application.streamingAssetsPath, "JSON/FuzzyLogicDefinitions.json");
        if (File.Exists(filePathDefinitions)) {
            dataAsJson = File.ReadAllText(filePathDefinitions);
            var attrMap = JsonUtility.FromJson<FuzzyAttributesMap>(dataAsJson);
            Logic.fuzzyAttributesMap = attrMap;
            // dataAsJson = File.ReadAllText(filePathIfThen);
            // Logic.ruleSetMap = JsonUtility.FromJson<RuleSetMap>(dataAsJson);
            ImportRulesFromCSV(Path.Combine(Application.streamingAssetsPath, "CSV"));
            var ruleMap = Logic.ruleSetMap.ruleSets;
            Debug.Log("Fuzzy logic successfully loaded");
            // Debug.Log(Logic.fuzzyAttributesMap.fuzzyAttributes["Speed"].fuzzySets["Slow"].GetValueAt(5));
            // Debug.Log(Logic.ifThenArray.ifThenStatements[0].inputs[0].name);
        }
        else {
            Debug.LogError("Cannot find fuzzy logic data!");
        }
    }

    private static void ImportRulesFromCSV(string path) {
        Logic.ruleSetMap = new RuleSetMap();
        var files = Directory.GetFiles(path, "*.csv");
        foreach (var f in files) {
            ReadCSV(f);
        }
    }

    private static void ReadCSV(string path) {
        if (File.Exists(path)) {
            string name = Path.GetFileNameWithoutExtension(path);
            //Debug.Log(name);
            RuleSet ruleSet = new RuleSet(name);
            string[] lines = File.ReadAllLines(path);
            ParseRuleSet(lines, ruleSet);
        }
    }

    private static void ParseRuleSet(string[] lines, RuleSet ruleSet) {
        if (lines.Length < 3)
            return;
        //ruleSet.ifThenStatements = new IfThen[lines.Length - 2];
        var ifThenStatements = new List<IfThen>();
        string[] parts = lines[0].Split(SEPARATOR);
        bool[] inputMask = new bool[parts.Length];
        for (int i = 0; i < parts.Length; i++) {
            if (parts[i] == INPUT)
                inputMask[i] = true;
        }
        var attributes = lines[1].Split(SEPARATOR);
        for (int i = 2; i < lines.Length; i++) {
            parts = lines[i].Split(SEPARATOR);
            IfThen ifThen = new IfThen();
            List<AttributeValue> inputs = new List<AttributeValue>();
            List<AttributeValue> outputs = new List<AttributeValue>();
            for (int j = 0; j < parts.Length; j++) {
                string part = parts[j].Trim();
                if (part == "")
                    continue;
                AttributeValue attributeValue = new AttributeValue(attributes[j], parts[j]);
                if (inputMask[j]) {
                    inputs.Add(attributeValue);
                }
                else {
                    outputs.Add(attributeValue);
                }
            }
            ifThen.inputs = inputs.ToArray();
            ifThen.outputs = outputs.ToArray();
            // Debug.Log(inputs.Count + " " + outputs.Count);
            if (inputs.Count < 1 || outputs.Count < 1)
                continue;
            ifThen.SetFuzzyAttributeReferences();
            ifThenStatements.Add(ifThen);
        }
        ruleSet.ifThenStatements = ifThenStatements.ToArray();
        ruleSet.CalculateMask();
        Logic.ruleSetMap.ruleSets.Add(ruleSet.name, ruleSet);
    }
}