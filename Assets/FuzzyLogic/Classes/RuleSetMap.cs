﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class RuleSetMap :ISerializationCallbackReceiver {
    
    [SerializeField]
    public RuleSet[] ruleSetsDef;
    
    [NonSerialized]
    public Dictionary<string, RuleSet> ruleSets = new Dictionary<string, RuleSet>();
    
    public void OnBeforeSerialize() { }

    public void OnAfterDeserialize() {
        for (int i = 0; i < ruleSetsDef.Length; i++) {
            ruleSets.Add(ruleSetsDef[i].name, ruleSetsDef[i]);
        }

        //fuzzyAttributesDef = null;
    }

    public override string ToString() {
        string str = "";
        foreach (var rule in ruleSets) {
            str = rule.Key + "\n" + rule.Value.ToString() + "\n";
        }

        return str;
    }
}
