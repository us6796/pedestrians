﻿public class TriangularFunction : MembershipFunction {
    private float l;
    private float r;
    private float m;

    public TriangularFunction(float l, float m, float r) {
        this.type = TRIANGULAR;
        this.l = l;
        this.r = r;
        this.m = m;
    }

    public override float GetValueAt(float x) {
        if (x >= l && x <= m) {
            return (x - l) / (m - l);
        }
        if (x <= r && x > m) {
            return (r - x) / (r - m);
        }
        return 0;
    }
}
