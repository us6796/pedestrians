﻿
using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

[Serializable]
public class FuzzyAttribute :ISerializationCallbackReceiver {
    public float start;
    public float end;
    public string name;
    public bool input;
    public bool output;
    [NonSerialized] public int index;
    
    // public FuzzySet[] fuzzySets;
    [NonSerialized]
    public Dictionary<string, FuzzySet> fuzzySets = new Dictionary<string, FuzzySet>();
    
    [SerializeField]
    public FuzzySet[] fuzzySetsDef;

    public void OnAfterDeserialize() {
        for (int i = 0; i < fuzzySetsDef.Length; i++) {
            var fuzzySet = fuzzySetsDef[i];
            fuzzySet.membershipFunction.start = start;
            fuzzySet.membershipFunction.end = end;
            fuzzySet.fuzzyAttribute = this;
            fuzzySet.index = i;
            if (output) {
                fuzzySet.membershipFunction.CalcDisretization();
            }
            fuzzySets.Add(fuzzySet.name, fuzzySet);
        }
        // fuzzySetsDef = null;
    }

    // public FuzzyAttributes(int start, int end, [NotNull] string name) {
    //     this.start = start;
    //     this.end = end;
    //     this.name = name ?? throw new ArgumentNullException(nameof(name));
    // }

    // public FuzzyAttributes(int start, int end, [NotNull] string name, [NotNull] FuzzySet[] fuzzySets) {
    //     this.start = start;
    //     this.end = end;
    //     this.name = name ?? throw new ArgumentNullException(nameof(name));
    //     this.fuzzySets = fuzzySets ?? throw new ArgumentNullException(nameof(fuzzySets));
    // }

    public void AddFuzzySet(FuzzySet fuzzySet) {
        // Array.Resize(ref fuzzySets, fuzzySets.Length + 1);
        // fuzzySets[fuzzySets.Length - 1] = fuzzySet;
        
        fuzzySets.Add(fuzzySet.name, fuzzySet);
        
    }

    public void OnBeforeSerialize() { }
    
}
