﻿using System;
using UnityEngine;

[Serializable]
public class IfThen : ISerializationCallbackReceiver {
    public AttributeValue[] inputs;
    public AttributeValue[] outputs;

    private FuzzyAttribute[] fuzzyAttributeInputs;
    private FuzzyAttribute[] fuzzyAttributeOutputs;
    
    private FuzzySet[] fuzzySetInputs;
    private FuzzySet[] fuzzySetOutputs;

    public void Evaluate(float[][] aggregates, float[][] fuzzyValues) {
        float min = FindMin(fuzzyValues);
        // foreach (var input in inputs) {
        //     Debug.Log(input.ToString());
        // }
        // Debug.Log(min);
        // Debug.Log("-----------");
        if (min == 0)
            return;
        for (int i = 0; i < fuzzyAttributeOutputs.Length; i++) {
            FuzzyAttribute output = fuzzyAttributeOutputs[i];
            for (int j = 0; j < Defines.DISCRETIZATION_SIZE; j++) {
                // float implicationValue = Math.Min(min, output.fuzzySets[outputs[i].set].GetDiscretization(j));
                float implicationValue = min * fuzzySetOutputs[i].GetDiscretization(j);
                // if (outputs[0].name == "ObstacleWeightFactor" && min > 0) {
                //     Debug.Log(fuzzySetOutputs[i].GetDiscretization(j));
                //     Debug.Log(implicationValue);
                // }
                aggregates[output.index][j] = Math.Max(aggregates[output.index][j], implicationValue);
            }
            
            // FuzzyAttribute output = fuzzyAttributeOutputs[i];
            // for (int j = 0; j < Defines.DISCRETIZATION_SIZE; j++) {
            //     // float implicationValue = Math.Min(min, output.fuzzySets[outputs[i].set].GetDiscretization(j));
            //     float implicationValue = min * output.fuzzySets[outputs[i].set].GetDiscretization(j);
            //     aggregates[output.index][j] = Math.Max(aggregates[output.index][j], implicationValue);
            // }
        }
    }
    
    public float FindMin(float[][] fuzzyValues) {
        float min = float.MaxValue;
        for (int i = 0; i < inputs.Length; i++) {
            FuzzyAttribute fuzzyAttribute = fuzzyAttributeInputs[i];
            // float value = fuzzyAttribute.fuzzySets[inputs[i].set]
            //     .GetValueAt(inputValues[fuzzyAttribute.index]);
            // Debug.Log(fuzzyValues.Length);
            // Debug.Log(fuzzyAttribute.name);
            // Debug.Log(fuzzySetInputs[i].index);
            // Debug.Log(fuzzyValues[fuzzyAttribute.index].Length);
            float value = fuzzyValues[fuzzyAttribute.index][fuzzySetInputs[i].index];
            if (value < min) {
                min = value;
            }
            // Debug.Log(inputs[i].name + " " + inputs[i].set + " = " + value);
        }
        return min;
    }
    
    public float FindMax(float[][] fuzzyValues) {
        float max = -1;
        for (int i = 0; i < inputs.Length; i++) {
            FuzzyAttribute fuzzyAttribute = fuzzyAttributeInputs[i];
            // float value = fuzzyAttribute.fuzzySets[inputs[i].set]
            //     .GetValueAt(inputValues[fuzzyAttribute.index]);
            float value = fuzzyValues[fuzzyAttribute.index][fuzzySetInputs[i].index];
            if (value > max) {
                max = value;
            }
            // Debug.Log(inputs[i].name + " " + inputs[i].set + " = " + value);
        }
        return max;
    }

    public float FindMax(float[] inputValues) {
        float max = -1;
        for (int i = 0; i < inputs.Length; i++) {
            FuzzyAttribute fuzzyAttribute = fuzzyAttributeInputs[i];
            float value = fuzzyAttribute.fuzzySets[inputs[i].set]
                .GetValueAt(inputValues[fuzzyAttribute.index]);
            if (value > max) {
                max = value;
            }
            // Debug.Log(inputs[i].name + " " + inputs[i].set + " = " + value);
        }
        return max;
    }

    public void OnBeforeSerialize() { }

    public void OnAfterDeserialize() {
        SetFuzzyAttributeReferences();
    }

    public void SetFuzzyAttributeReferences() {
        fuzzyAttributeInputs = new FuzzyAttribute[inputs.Length];
        fuzzySetInputs = new FuzzySet[inputs.Length];
        for (int i = 0; i < inputs.Length; i++) {
            fuzzyAttributeInputs[i] = Logic.fuzzyAttributesMap.GetFuzzyAttribute(inputs[i].name);
            fuzzySetInputs[i] = fuzzyAttributeInputs[i].fuzzySets[inputs[i].set];
        }
        fuzzyAttributeOutputs = new FuzzyAttribute[outputs.Length];
        fuzzySetOutputs = new FuzzySet[outputs.Length];
        for (int i = 0; i < outputs.Length; i++) {
            fuzzyAttributeOutputs[i] = Logic.fuzzyAttributesMap.GetFuzzyAttribute(outputs[i].name);
            fuzzySetOutputs[i] = fuzzyAttributeOutputs[i].fuzzySets[outputs[i].set];
        }
    }

    public override string ToString() {
        string str = "";
        foreach (var input in inputs) {
            str += input + " ";
        }
        foreach (var output in outputs) {
            str += output + " ";
        }
        return str;
    }
}
