﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class RuleSet : ISerializationCallbackReceiver{
    public string name;
    public IfThen[] ifThenStatements;
    private bool[] inputMask;
    private bool[] outputMask;

    private float[][] fuzzyValues;
    private float[][] aggregates;

    public RuleSet(string name) {
        this.name = name;
    }

    public float[] Evaluate(float[] inputValues) {
        var fuzzyAttributes = Logic.fuzzyAttributesMap.fuzzyAttributesDef;
        for (int i = 0; i < fuzzyValues.Length; i++) {
            if (!fuzzyAttributes[i].input || !inputMask[i]) //
                continue;
            for (int j = 0; j < fuzzyValues[i].Length; j++) {
                fuzzyValues[i][j] = fuzzyAttributes[i].fuzzySetsDef[j].GetValueAt(inputValues[i]);
                // if (fuzzyValues[i][j] != 0 && name == "R5")
                //     Debug.Log(fuzzyAttributes[i].name + ": " + fuzzyAttributes[i].fuzzySetsDef[j].name + 
                //               " = " + fuzzyValues[i][j]);
            }
        }
        //List<float>[] aggregates = new List<float>[fuzzyAttributes.Count];
        for (int i = 0; i < fuzzyAttributes.Length; i++) {
            if (outputMask[i])
                Array.Clear(aggregates[i], 0, aggregates[i].Length);
        }
        foreach (var t in ifThenStatements) {
            t.Evaluate(aggregates, fuzzyValues);
        }
        float[] result = new float[fuzzyAttributes.Length];
        for (int i = 0; i < fuzzyAttributes.Length; i++) {
            if (!outputMask[i]) {
                result[i] = -1;
                continue;
            }
            result[i] = Centroid(aggregates, fuzzyAttributes[i].index, fuzzyAttributes[i].start, fuzzyAttributes[i].end);
        }

        return result;
    }

    private static float Centroid(float[][] aggregates, int index, float start, float end) {
        float upper = 0;
        float lower = 0;
        float length = end - start;
        for (int i = 0; i < Defines.DISCRETIZATION_SIZE; i++) {
            float value = start + length * i / (Defines.DISCRETIZATION_SIZE - 1);
            // Debug.Log(value + " " +aggregates[index, i]);
            upper += aggregates[index][i] * value;
            lower += aggregates[index][i];
        }

        if (lower == 0)
            return 0;
        // #Debug.Log( upper + " " + lower);
        return upper / lower;
    }

    public void OnBeforeSerialize() { }

    public void OnAfterDeserialize() {
        CalculateMask();
    }

    public void CalculateMask() {
        var fuzzyAttributes = Logic.fuzzyAttributesMap;
        inputMask = new bool[fuzzyAttributes.GetFuzzyAttributeCount()];
        outputMask = new bool[fuzzyAttributes.GetFuzzyAttributeCount()];
        foreach (var ifThenStatement in ifThenStatements) {
            foreach (var input in ifThenStatement.inputs) {
                inputMask[fuzzyAttributes.GetFuzzyAttribute(input.name).index] = true;
            }
            foreach (var output in ifThenStatement.outputs) {
                outputMask[fuzzyAttributes.GetFuzzyAttribute(output.name).index] = true;
            }
        }
        
        var fuzzyAttributesArr = Logic.fuzzyAttributesMap.fuzzyAttributesDef;
        fuzzyValues = new float[fuzzyAttributesArr.Length][];
        aggregates = new float[fuzzyAttributesArr.Length][];
        for (int i = 0; i < fuzzyValues.Length; i++) {
            if (fuzzyAttributesArr[i].input && inputMask[i]) {
                // Debug.Log(fuzzyAttributesArr[i].name + " " + fuzzyAttributesArr[i].fuzzySets.Count);
                fuzzyValues[i] = new float[fuzzyAttributesArr[i].fuzzySets.Count];
            }
            if (fuzzyAttributesArr[i].output && outputMask[i])
                aggregates[i] = new float[Defines.DISCRETIZATION_SIZE];
        }
    }

    public override string ToString() {
        string str = "";
        foreach (var ifThen in ifThenStatements) {
            str += ifThen + "\n";
        }

        return str;
    }
}
