﻿using System;
using JetBrains.Annotations;

[Serializable]
public class MembershipFunctionDefinition {
    public static readonly string TRAPEZOID = "Trapezoid";
    public static readonly string TRIANGULAR = "Triangular";
    
    public string type;
    public float l;
    public float ml;
    public float mr;
    public float r;
    public float m;
}