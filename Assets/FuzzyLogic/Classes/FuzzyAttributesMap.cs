﻿
using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

[Serializable]
public class FuzzyAttributesMap :ISerializationCallbackReceiver {
    [SerializeField]
    public FuzzyAttribute[] fuzzyAttributesDef;
    
    [NonSerialized]
    private Dictionary<string, FuzzyAttribute> fuzzyAttributes = new Dictionary<string, FuzzyAttribute>();
    
    public int GetFuzzyAttributeCount() {
        return fuzzyAttributes.Count;
    }
    public FuzzyAttribute GetFuzzyAttribute(string name) {
        return fuzzyAttributes[name];
    }

    public void OnBeforeSerialize() { }

    public void OnAfterDeserialize() {
        for (int i = 0; i < fuzzyAttributesDef.Length; i++) {
            fuzzyAttributesDef[i].index = i;
            fuzzyAttributes.Add(fuzzyAttributesDef[i].name, fuzzyAttributesDef[i]);
        }

        //fuzzyAttributesDef = null;
    }
}