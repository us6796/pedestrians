﻿using System;
using JetBrains.Annotations;
using UnityEngine;

[Serializable]
public class FuzzySet :ISerializationCallbackReceiver {
    public string name;
    [NonSerialized]
    public int index;
    [NonSerialized]
    public FuzzyAttribute fuzzyAttribute;
    // private int index;
    
    [SerializeField] 
    private MembershipFunctionDefinition membershipFunctionDef;
    
    [NonSerialized]
    public MembershipFunction membershipFunction;
    
    [SerializeField]
    private string copyOf;

    public FuzzySet([NotNull] string name,
        [NotNull] MembershipFunction membershipFunction) {
        this.name = name ?? throw new ArgumentNullException(nameof(name));
        // this.index = index;
        this.membershipFunction = membershipFunction ?? throw new ArgumentNullException(nameof(membershipFunction));
    }
    
    public float GetValueAt(float x) {
        return membershipFunction.GetValueAt(x);
    }
    
    public float GetDiscretization(int x) {
        return membershipFunction.discretization[x];
    }

    public void OnBeforeSerialize() {}

    public void OnAfterDeserialize() {
        if (membershipFunctionDef.type.Equals(MembershipFunctionDefinition.TRAPEZOID)) {
            membershipFunction = new TrapezoidFunction(membershipFunctionDef.l, 
                membershipFunctionDef.ml, membershipFunctionDef.mr, membershipFunctionDef.r);
        }
        else if (membershipFunctionDef.type.Equals(MembershipFunctionDefinition.TRIANGULAR)) {
            membershipFunction = new TriangularFunction(membershipFunctionDef.l, 
                membershipFunctionDef.m, membershipFunctionDef.r);
        }
    }
}
