﻿public class TrapezoidFunction : MembershipFunction {
    private float l;
    private float ml;
    private float mr;
    private float r;

    public TrapezoidFunction(float l, float ml, float mr, float r) {
        this.type = TRAPEZOID;
        this.l = l;
        this.ml = ml;
        this.mr = mr;
        this.r = r;
    }

    public override float GetValueAt(float x) {
        if (x <= l || x >= r) return 0;
        if (x < ml) {
            return (x - l) / (ml - l);
        }
        if (x > mr) {
            return (r - x) / (r - mr);
        }
        return 1;
    }
}
