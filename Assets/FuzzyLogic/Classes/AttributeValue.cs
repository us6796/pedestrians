﻿using System;

[Serializable]
public class AttributeValue {
    public string name;
    public string set;

    public AttributeValue(string name, string set) {
        this.name = name;
        this.set = set;
    }

    public override string ToString() {
        return name + ": " + set;
    }
}
