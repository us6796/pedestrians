﻿public abstract class MembershipFunction {
    public static readonly int TRAPEZOID = 0;
    public static readonly int TRIANGULAR = 1;

    protected int type;
    public float start;
    public float end;

    public float[] discretization;

    public abstract float GetValueAt(float x);

    public void CalcDisretization() {
        discretization = new float[Defines.DISCRETIZATION_SIZE];
        float length = end - start;
        discretization[0] = GetValueAt(start);
        for (int i = 1; i < Defines.DISCRETIZATION_SIZE - 1; i++) {
            discretization[i] = GetValueAt(start + length * i / (Defines.DISCRETIZATION_SIZE - 1));
        }
        discretization[Defines.DISCRETIZATION_SIZE - 1] = GetValueAt(end);
    }
}
