﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Loader : MonoBehaviour {

    private const char SEPARATOR = ';';
    private const string INPUT = "I";
    private const string OUTPUT = "O";
    
    public enum NextScene { TestingGround, Square, Obstacle };

    public NextScene nextScene;

    public int testRepeats = 10;

    void Awake() {
        Logic.testRepeats = testRepeats;
        FuzzyLogicLoader.Load();
        // SceneManager.LoadScene("TestingGround");
        if (nextScene == NextScene.TestingGround)
            SceneManager.LoadScene("TestingGround");
        else if (nextScene == NextScene.Square)
            SceneManager.LoadScene("Square");
        else if (nextScene == NextScene.Obstacle)
            SceneManager.LoadScene("Obstacle");
    }
}