﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainController : MonoBehaviour {
    [Range (0f, 1f)]public float timestep = 0.1f;
    public bool usePersonality;
    public bool use3DModel = true;
    public bool writeOutputs;
    [SerializeField] private string outputFileName = "output";
    public float preferredVelocity;
    private float exitWidth = 2.5f;
    public string comment;
    public bool overrideSpawners = true;
    private static bool paused = true;
    [SerializeField] private Text playButtonText;
    private GameObject pedestrian;
    [SerializeField] private GameObject pedestrian2D;
    [SerializeField] private GameObject pedestrian3D;
    [SerializeField] private Text time;
    [SerializeField] private Text NAgents;
    
    private Color positiveColor = new Color(0, 255, 0, 1);
    private Color negativeColor = new Color(255, 0, 0, 1);

    private float timeLapse = 0f;
    [Range (0f, 1f)][SerializeField] public float personalityPercentage = 0.25f;
    [Range (-100f, 100f)][SerializeField] public float minOpenness = -100;
    [Range (-100f, 100f)][SerializeField] public float maxOpenness = 100;
    
    [Range (-100f, 100f)][SerializeField] public float minConscientiousness = -100;
    [Range (-100f, 100f)][SerializeField] public float maxConscientiousness = 100;
    
    [Range (-100f, 100f)][SerializeField] public float minExtraversion = -100;
    [Range (-100f, 100f)][SerializeField] public float maxExtraversion = 100;
    
    [Range (-100f, 100f)][SerializeField] public float minAgreeableness = -100;
    [Range (-100f, 100f)][SerializeField] public float maxAgreeableness = 100;
    
    [Range (-100f, 100f)][SerializeField] public float minNeuroticism = -100;
    [Range (-100f, 100f)][SerializeField] public float maxNeuroticism = 100;

    public List<PedestrianMovementController> pedestrians;
    private Spawner[] spawners;

    private int nPedestrians; 

    private static string SEPARATOR = ";";
    private string outputPath = "/output";
    // private string fileName = "output.csv";

    private float avgSpeed = 0;

    void Start() {
        if (use3DModel) {
            pedestrian = pedestrian3D;
        }
        else {
            pedestrian = pedestrian2D;
        }
        paused = Logic.testRepeats < 0;
        pedestrians = new List<PedestrianMovementController>();
        GeneratePedestrians();
        outputPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
                     + Path.DirectorySeparatorChar + "Pedestrians";
        Debug.Log("Output path: " + outputPath);
        // Write file using StreamWriter  
      
        // pedestrians[0].debug = true;
    }

    // Update is called once per frame
    void Update() {
       
        if (!paused) {
            // Debug.Log("Update");
            for (int i = 0; i < pedestrians.Count; i++) {
                var pedestrian = pedestrians[i];
                // if (!pedestrian.gameObject.activeSelf)
                //     continue;
                pedestrian.UpdateVision();
                if (pedestrian.goalDistance < 0.2) {
                    // pedestrian.gameObject.SetActive(false);
                    pedestrians.RemoveAt(i);
                    i--;
                    Destroy(pedestrian.gameObject);
                    if (NAgents)
                        NAgents.text = pedestrians.Count.ToString();
                }
            }

            foreach (var pedestrian in pedestrians) {
                // if (!pedestrian.gameObject.activeSelf)
                //     continue;
                pedestrian.Move();
            }

            timeLapse += Time.deltaTime * timestep;
            if (!time) {
                time.text = timeLapse.ToString("F");
            }
            if (pedestrians.Count == 0) {
                if (writeOutputs) {
                    outputToFile();
                }
                paused = true;
                if (Logic.testRepeats > 1) {
                    Logic.testRepeats--;
                    Restart();
                }
            }
        }
    }

    // private void FindPedestrians() {
    //     var pedestriansObjects = GameObject.FindGameObjectsWithTag("Pedestrian");
    //     pedestrians = new List<PedestrianMovementController>(pedestriansObjects.Length);
    //     foreach (var t in pedestriansObjects) {
    //         pedestrians.Add(t.GetComponent<PedestrianMovementController>());
    //     }
    //     pedestrians[0] = GameObject.Find("Dot").GetComponent<PedestrianMovementController>();
    //     pedestrians[1] = GameObject.Find("Janez").GetComponent<PedestrianMovementController>();
    // }

    private void GeneratePedestrians() {
        var spawnerObjects = GameObject.FindGameObjectsWithTag("Spawner");
        spawners = new Spawner[spawnerObjects.Length];
        for (int i = 0; i < spawnerObjects.Length; i++) {
            var spawner = spawnerObjects[i].GetComponent<Spawner>();
            spawners[i] = spawner;
            spawner.GeneratePedestrians(pedestrian, this);
        }

        avgSpeed = 0;
        foreach (var pedestrian in pedestrians) {
            pedestrian.SetReferences(this);
            avgSpeed += pedestrian.maxVelocity;
        }

        avgSpeed /= pedestrians.Count;

        nPedestrians = pedestrians.Count;
        if (NAgents)
            NAgents.text = nPedestrians.ToString();
    }

    public void PlayPause() {
        if (paused) {
            paused = false;
            playButtonText.text = "Pause";
        }
        else {
            paused = true;
            playButtonText.text = "Play";
        }
    }
    
    public void Exit() {
        Application.Quit();
    }
    
    public void Restart() {
        string currentSceneName = SceneManager.GetActiveScene().name;
        SceneManager.LoadScene(currentSceneName);
    }

    public void SetFilter(int value) {
        Debug.Log(value);
        foreach (var pedestrian in pedestrians) {
            if (value == 0) {
                pedestrian.ResetColor();
                continue;
            }
            float parameter = 0;
            switch (value) {
                case 1: parameter = pedestrian.openness;
                    break;
                case 2: parameter = pedestrian.conscientiousness;
                    break;
                case 3: parameter = pedestrian.extraversion;
                    break;
                case 4: parameter = pedestrian.agreeableness;
                    break;
                case 5: parameter = pedestrian.neuroticism;
                    break;
            }

            parameter /= 100;
            Debug.Log(parameter);
            Color color;
            if (parameter > 0) {
                color = parameter * positiveColor;
                color.a = 1;
                Debug.Log(color);
            }
            else {
                parameter *= -1;
                color = parameter * negativeColor;
                color.a = 1;
            }
            pedestrian.SetColor(color);
        }
    }

    void outputToFile() {
        string filePath = outputPath + Path.DirectorySeparatorChar + outputFileName + ".csv";
        // Write file using StreamWriter  
        if (!Directory.Exists(outputPath)) {
            Directory.CreateDirectory(outputPath);
        }
        if (!File.Exists(filePath)) {	
            File.CreateText(filePath);
        }

        float avgSpeed = preferredVelocity;
        if (usePersonality) {
            avgSpeed = this.avgSpeed;
        }

        if (File.Exists(filePath)) {
            using (StreamWriter writer = File.AppendText(filePath)) {  
                string date = DateTime.Now.ToString("yyyy-MM-dd");
                string scene = SceneManager.GetActiveScene().name;
                string value = timeLapse.ToString(CultureInfo.InvariantCulture);
                string speed = avgSpeed.ToString(CultureInfo.InvariantCulture);
                string personality = usePersonality.ToString();
                string exitWidth = this.exitWidth.ToString(CultureInfo.InvariantCulture);
                string toWrite = date + SEPARATOR + scene + SEPARATOR + personality + 
                                 SEPARATOR + speed + SEPARATOR + exitWidth + SEPARATOR + value + SEPARATOR + comment;
                writer.WriteLine(toWrite);
            }  
        }
    }
}
