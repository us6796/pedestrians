﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class PedestrianMovementController : MonoBehaviour {
    // private BoxCollider[] wallColliders;
    // public static readonly float VIEW_ANGLE = 60;
    public bool debug;
    public const float SECTOR_R_START = -85;
    public const float SECTOR_FR_START = -45;
    public const float SECTOR_F_START = -15;
    public const float SECTOR_FL_START = 15;
    public const float SECTOR_L_START = 45;
    public const float SECTOR_L_END = 85;
    public const float RAYS_PER_10_DEG = 5;
    public const float SECTOR_R_RAYS = (SECTOR_FR_START - SECTOR_R_START) / 10 * RAYS_PER_10_DEG;
    public const float SECTOR_FR_RAYS = (SECTOR_F_START - SECTOR_FR_START) / 10 * RAYS_PER_10_DEG ;
    public const float SECTOR_F_RAYS = (SECTOR_FL_START - SECTOR_F_START) / 10 * RAYS_PER_10_DEG;
    public const float SECTOR_FL_RAYS = (SECTOR_L_START - SECTOR_FL_START) / 10 * RAYS_PER_10_DEG;
    public const float SECTOR_L_RAYS = (SECTOR_L_END - SECTOR_L_START) / 10 * RAYS_PER_10_DEG;
    public const float RADIUS = 0.2f;
    public const float VIEW_DISTANCE = 5 + RADIUS;

    public static float hitrost = 0; //tule kllicem kot speed v animator state controller
    public static float maxHitrost = 0;
    public static float kot = 0;
    
    // public const float RADIUS = 0.2f;
    // public const float VIEW_DISTANCE = 5 + radius;
    public const float DEFAULT_RADIUS = 0.2f;
    public const float DEFAULT_VIEW_DISTANCE = 5;
    public const int DEFAULT_VISION_LIMIT = 7;
    public const int DEFAULT_MAX_VELOCITY = 5;
    public const float DEFAULT_OBSTACLE_WEIGHT_FACTOR = 0.5f;

    public int visionLimit = 7;
    private Collider2D goal;
    
    public float speed;
    public float angle;
    public float goalDistance;
    
    public float speedObstacleAvoidance;
    public float angleObstacleAvoidance = 0;
    public float speedNegativeEnergy;
    public float angleNegativeEnergy;
    public float speedGoal;
    public float angleGoal;
    
    public float[] obstacleDistances;
    public float[] obstacleImpacts;
    public float[] collisionRisks;
    public float[] negativeEnergies;
    
    public float weightO;
    public float weightN;
    public float weightG;

   
    private MainController mainController;
    private List<PedestrianMovementController> pedestrians;
    
    // private int closestPedestriansCount;
    // private PedestrianMovementController[] closestPedestrians;
    // private bool[] closestPedestrianMask;
    
    private int filteredPedestriansCount;
    private Pedestrian[] filteredPedestrians;

    private float[] inputs;

    public float openness;
    public float conscientiousness;
    public float extraversion;
    public float agreeableness;
    public float neuroticism;
    
    public float maxNeighbors;
    public float maxVelocity;
    public float neighborDistance;
    public float radius;
    public float obstacleWeightFactor = 0.5f;

    public float viewDistance;

    private SpriteRenderer sprite;
    private Color defaultColor;

    private bool usePersonality = false;
    // private float[] outputs;

    // private static readonly Vector3 FlatVector = new Vector3(1, 0, 0);
    // Start is called before the first frame update
    void Start() {
        hitrost = 0;
        obstacleDistances = new float[5];
        obstacleImpacts = new float[5];
        collisionRisks = new float[5];
        negativeEnergies = new float[5];
        // closestPedestrians = new List<PedestrianMovementController>(7);
        
        
        // outputs = new float[fuzzyAttributeMap.GetFuzzyAttributeCount()];
    }

    public void SetParameters(Color color, GameObject goalObject, float prefSpeed) {
        inputs = new float[Logic.fuzzyAttributesMap.GetFuzzyAttributeCount()];
        sprite = GetComponent<SpriteRenderer>();
        defaultColor = color;
        SetColor(color);
        goal = goalObject.GetComponent<Collider2D>();
        if (prefSpeed > 0) {
            maxVelocity = prefSpeed;
        } else {
            maxVelocity = DEFAULT_MAX_VELOCITY;
        }
    }

    public void SetColor(Color color) {
        sprite.color = color;
    }
    
    public void ResetColor() {
        sprite.color = defaultColor;
    }
    
    public void SetPersonality(bool usePersonality) {
        this.usePersonality = usePersonality;
        this.openness = 0;
        this.conscientiousness = 0;
        this.extraversion = 0;
        this.agreeableness = 0;
        this.neuroticism = 0;
    }

    public void SetPersonality(float openness, float conscientiousness, 
        float extraversion, float agreeableness, float neuroticism) {
        this.openness = openness;
        this.conscientiousness = conscientiousness;
        this.extraversion = extraversion;
        this.agreeableness = agreeableness;
        this.neuroticism = neuroticism;
    }
    
    private static float WrapAngle(float angle)
    {
        angle%=360;
        if(angle >180)
            return angle - 360;
 
        return angle;
    }
    
    public void SetReferences(MainController mainController) {
        this.mainController = mainController;
        pedestrians = mainController.pedestrians;

        // closestPedestrianMask = new bool [pedestrians.Count];
        
        // closestPedestrians = new PedestrianMovementController[visionLimit];
        
        filteredPedestrians = new Pedestrian[pedestrians.Count];
        for (int i = 0; i < filteredPedestrians.Length; i++){
            filteredPedestrians[i] = new Pedestrian();
        }

        CalculatePersonalityEffects();
        
        visionLimit = Math.Min(visionLimit, pedestrians.Count);
    }

    private void CalculatePersonalityEffects() {
        if (!mainController.usePersonality) {
            radius = DEFAULT_RADIUS;
            neighborDistance = DEFAULT_VIEW_DISTANCE;
            viewDistance = DEFAULT_VIEW_DISTANCE + DEFAULT_RADIUS;
            visionLimit = DEFAULT_VISION_LIMIT;
            obstacleWeightFactor = DEFAULT_OBSTACLE_WEIGHT_FACTOR;
            // maxVelocity = DEFAULT_MAX_VELOCITY;
            return;
        }
        var fuzzyAttributeMap = Logic.fuzzyAttributesMap;
        
        inputs[fuzzyAttributeMap.GetFuzzyAttribute("Openness").index] = openness;
        inputs[fuzzyAttributeMap.GetFuzzyAttribute("Conscientiousness").index] = conscientiousness;
        inputs[fuzzyAttributeMap.GetFuzzyAttribute("Extraversion").index] = extraversion;
        inputs[fuzzyAttributeMap.GetFuzzyAttribute("Agreeableness").index] = agreeableness;
        inputs[fuzzyAttributeMap.GetFuzzyAttribute("Neuroticism").index] = neuroticism;
        
        maxNeighbors = Logic.ruleSetMap.ruleSets["MaxNeighbors"]
            .Evaluate(inputs)[fuzzyAttributeMap.GetFuzzyAttribute("MaxNeighbors").index];
        maxVelocity = Logic.ruleSetMap.ruleSets["MaxVelocity"]
            .Evaluate(inputs)[fuzzyAttributeMap.GetFuzzyAttribute("MaxVelocity").index];
        neighborDistance = Logic.ruleSetMap.ruleSets["NeighborDist"]
            .Evaluate(inputs)[fuzzyAttributeMap.GetFuzzyAttribute("NeighborDist").index];
        radius = Logic.ruleSetMap.ruleSets["Radius"]
            .Evaluate(inputs)[fuzzyAttributeMap.GetFuzzyAttribute("Radius").index];
        obstacleWeightFactor = Logic.ruleSetMap.ruleSets["ObstacleWeightFactor"]
            .Evaluate(inputs)[fuzzyAttributeMap.GetFuzzyAttribute("ObstacleWeightFactor").index];
        
        viewDistance = neighborDistance + radius;
        visionLimit = (int) maxNeighbors;
        
    }

    // Update is called once per frame
    void Update() {
        // Scan()
        //   kot = angle;
     //   Debug.Log(kot);
        if (debug)
            ShowPerception(SECTOR_R_START, SECTOR_L_END, (SECTOR_L_END - SECTOR_R_START) / 10 * RAYS_PER_10_DEG);
        // if (Input.GetKeyDown("space")) {
        //     UpdateVision();
        // }
    }
    public void UpdateVision() {
        // float angleObstacleAvoidance = 0;
        // float speedObstacleAvoidance = 0;
        // float angleGoal = 0;
        // float speedGoal = 0;
        // float angleNegativeEnergy = 0;
        // float speedNegativeEnergy = 0;
        // float goalDistance = 0;

        ScanSector(SECTOR_R_START, SECTOR_FR_START, SECTOR_R_RAYS, 4);
        ScanSector(SECTOR_FR_START, SECTOR_F_START, SECTOR_FR_RAYS, 3);
        ScanSector(SECTOR_F_START, SECTOR_FL_START, SECTOR_F_RAYS, 2);
        ScanSector(SECTOR_FL_START, SECTOR_L_START, SECTOR_FL_RAYS, 1);
        ScanSector(SECTOR_L_START, SECTOR_L_END, SECTOR_L_RAYS, 0);
        
        // obsImpact = ;obstacleImpacts
        
        EvaluateLocalObstacles();
        EvaluateGoalBehavior(ref speedGoal, ref angleGoal, ref goalDistance);
        ScanPedestrians();
        collisionRisks[0] = Mathf.Clamp(collisionRisks[0], 0, 1);
        collisionRisks[1] = Mathf.Clamp(collisionRisks[1], 0, 1);
        collisionRisks[2] = Mathf.Clamp(collisionRisks[2], 0, 1);
        collisionRisks[3] = Mathf.Clamp(collisionRisks[3], 0, 1);
        collisionRisks[4] = Mathf.Clamp(collisionRisks[4], 0, 1);
        float neF = CalcNegativeEnergyImpact();
        // ShowSpeedAndAngle(angleObstacleAvoidance, speedObstacleAvoidance, Color.cyan);
        // ShowSpeedAndAngle(speedNegativeEnergy, angleNegativeEnergy, Color.green);
        // ShowSpeedAndAngle(speedGoal, angleGoal, Color.yellow);
        //Calc weights
        var fuzzyAttributeMap = Logic.fuzzyAttributesMap;
        // inputs[fuzzyAttributeMap.GetFuzzyAttribute("Distance F").index] = obstacleDistances[2] / neighborDistance * 5;
        inputs[fuzzyAttributeMap.GetFuzzyAttribute("Distance F").index] = obstacleDistances[2];
        inputs[fuzzyAttributeMap.GetFuzzyAttribute("NE F").index] = neF;
        inputs[fuzzyAttributeMap.GetFuzzyAttribute("Goal Distance").index] = goalDistance;
        float [] outputs = Logic.ruleSetMap.ruleSets["R5f"].Evaluate(inputs);
        weightO = outputs[fuzzyAttributeMap.GetFuzzyAttribute("Weight O").index];
        weightN = outputs[fuzzyAttributeMap.GetFuzzyAttribute("Weight N").index];
        weightG = outputs[fuzzyAttributeMap.GetFuzzyAttribute("Weight G").index];
        // float normalizer = weightO + weightG;
        // angle = (weightO * angleObstacleAvoidance + weightG * angleGoal) / normalizer;
        // speed = (weightO * speedObstacleAvoidance + weightG * speedGoal) / normalizer;
        float normalizer = weightO + weightN + weightG;
        if (normalizer == 0) {
            angle = 0;
            speed = 0;
        }
        else {
            angle = (weightO * angleObstacleAvoidance + weightN * angleNegativeEnergy + weightG * angleGoal) / normalizer;
            speed = (weightO * speedObstacleAvoidance + weightN * speedNegativeEnergy + weightG * speedGoal) / normalizer;
        }

        if (debug) {
            ShowSpeedAndAngle(weightO * speedObstacleAvoidance, angleObstacleAvoidance, Color.cyan);
            ShowSpeedAndAngle(weightN * speedNegativeEnergy, angleNegativeEnergy, Color.green);
            ShowSpeedAndAngle(weightG * speedGoal, angleGoal, Color.yellow);
        }
        
        // this.weightG = weightG;
        // this.weightO = weightO;
        // this.weightN = weightN;
        if (debug)
            Debug.Log("--------");
        // angle = 0;
        // speed = 0;
        // this.goalDistance = goalDistance;
        // this.speedObstacleAvoidance = speedObstacleAvoidance;
        // this.speedNegativeEnergy = speedNegativeEnergy;
        // this.speedGoal = speedGoal;
    }

    void EvaluateLocalObstacles() {
        var fuzzyAttributeMap = Logic.fuzzyAttributesMap;
        // inputs[fuzzyAttributeMap.GetFuzzyAttribute("Distance L").index] = obstacleDistances[0] / neighborDistance * 5;
        // inputs[fuzzyAttributeMap.GetFuzzyAttribute("Distance LF").index] = obstacleDistances[1] / neighborDistance * 5;
        // inputs[fuzzyAttributeMap.GetFuzzyAttribute("Distance F").index] = obstacleDistances[2] / neighborDistance * 5;
        // inputs[fuzzyAttributeMap.GetFuzzyAttribute("Distance RF").index] = obstacleDistances[3] / neighborDistance * 5;
        // inputs[fuzzyAttributeMap.GetFuzzyAttribute("Distance R").index] = obstacleDistances[4] / neighborDistance * 5;
        inputs[fuzzyAttributeMap.GetFuzzyAttribute("Distance L").index] = obstacleDistances[0];
        inputs[fuzzyAttributeMap.GetFuzzyAttribute("Distance LF").index] = obstacleDistances[1];
        inputs[fuzzyAttributeMap.GetFuzzyAttribute("Distance F").index] = obstacleDistances[2];
        inputs[fuzzyAttributeMap.GetFuzzyAttribute("Distance RF").index] = obstacleDistances[3];
        inputs[fuzzyAttributeMap.GetFuzzyAttribute("Distance R").index] = obstacleDistances[4];
        float [] outputs = Logic.ruleSetMap.ruleSets["R0v2"].Evaluate(inputs);
        speedObstacleAvoidance = outputs[fuzzyAttributeMap.GetFuzzyAttribute("Speed").index];
        angleObstacleAvoidance = outputs[fuzzyAttributeMap.GetFuzzyAttribute("Direction").index];
    }

    void EvaluateGoalBehavior(ref float speedGoal, ref float angleGoal, ref float goalDistance) {
        var position = transform.position;
        Vector3 closestPoint = goal.ClosestPoint(position);
        Vector3 direction = closestPoint - position;
        float angle = Vector3.SignedAngle(transform.rotation * Vector3.right, direction, Vector3.forward);
        float distance = Mathf.Clamp(direction.magnitude, 0, 10);
        var fuzzyAttributeMap = Logic.fuzzyAttributesMap;
        inputs[fuzzyAttributeMap.GetFuzzyAttribute("Goal Angle").index] = angle;
        inputs[fuzzyAttributeMap.GetFuzzyAttribute("Goal Distance").index] = distance;
        float [] outputs = Logic.ruleSetMap.ruleSets["R4"].Evaluate(inputs);
        speedGoal = outputs[fuzzyAttributeMap.GetFuzzyAttribute("Speed").index];
        angleGoal = outputs[fuzzyAttributeMap.GetFuzzyAttribute("Direction").index];
        goalDistance = distance;
    }

    private void ScanPedestrians() {
        collisionRisks[0] = collisionRisks[1] = collisionRisks[2] = collisionRisks[3] = collisionRisks[4] = 0;
        var transform = this.transform;
        var forward = transform.rotation * Vector3.right;
        // var position = transform.position;
        var fuzzyAttributeMap = Logic.fuzzyAttributesMap;
        // closestPedestriansCount = SetClosestPedestrians();
        
        FilterPedestrians();
        Array.Sort(filteredPedestrians, 0, filteredPedestriansCount, new PedestrianComparer());

        int trueLimit = Math.Min(filteredPedestriansCount, visionLimit);
        // Debug.Log("Calc for: " + name);
        for (int i = 0; i < trueLimit; i++) {
            var pedestrian = filteredPedestrians[i];
            // Debug.Log("Seeing: " + pedestrian.controller.name);
            float distance = pedestrian.distance;
            float pedestrianAngle = pedestrian.pedestrianAngle;
            
            if (SectorFull(pedestrianAngle))
                continue;
            
            var pedestrianTransform = pedestrian.controller.transform;

            var pedestrianDirection = pedestrianTransform.rotation * Vector3.right;
            
            float movementAngle = Vector3.SignedAngle(forward, pedestrianDirection, Vector3.forward);
            if (pedestrianAngle > 0) {
                movementAngle *= -1;
            }

            // float trueDistance = Mathf.Clamp(((distance - 2 * radius) / neighborDistance) * 5, 0, neighborDistance);
            
            float trueDistance = Mathf.Clamp(distance - 2 * radius, 0, DEFAULT_VIEW_DISTANCE);

            inputs[fuzzyAttributeMap.GetFuzzyAttribute("PAngle").index] = movementAngle;
            inputs[fuzzyAttributeMap.GetFuzzyAttribute("PSpeed").index] = pedestrian.controller.speed;
            inputs[fuzzyAttributeMap.GetFuzzyAttribute("PDistance").index] = trueDistance;
            float cr = Logic.ruleSetMap.ruleSets["R2"].Evaluate(inputs)[fuzzyAttributeMap.GetFuzzyAttribute("CR").index];
            
            if (debug)
                Debug.Log(pedestrian.controller.gameObject.name + " " + movementAngle + " | CR: " + cr);
            
            if (pedestrianAngle < SECTOR_FR_START) {
                collisionRisks[4] += cr;
            } else if (pedestrianAngle < SECTOR_F_START) {
                collisionRisks[3] += cr;
            } else if (pedestrianAngle < SECTOR_FL_START) {
                collisionRisks[2] += cr;
            } else if (pedestrianAngle < SECTOR_L_START) {
                collisionRisks[1] += cr;
            } else {
                collisionRisks[0] += cr;
            }
        }

        // this.collisionRisks = collisionRisks;
        // return collisionRisks;
    }

    private bool SectorFull(float angle) {
        if (angle < SECTOR_FR_START) {
            if (collisionRisks[4] >= 1)
                return true;
        } else if (angle < SECTOR_F_START) {
            if (collisionRisks[3] >= 1)
                return true;
        } else if (angle < SECTOR_FL_START) {
            if (collisionRisks[2] >= 1)
                return true;
        } else if (angle < SECTOR_L_START) {
            if (collisionRisks[1] >= 1)
                return true;
        } else {
            if (collisionRisks[0] >= 1)
                return true;
        }
        return false;
    }

    // private int SetClosestPedestrians() {
    //     var transform = this.transform;
    //     var forward = transform.rotation * Vector3.right;
    //     var position = transform.position;
    //     // closestPedestrianMask = new bool[pedestrians.Count];
    //     Array.Clear(closestPedestrianMask, 0, closestPedestrianMask.Length);
    //     int closestPedestrianCount = 0;
    //     for (int i = 0; i < visionLimit; i++) {
    //         float min = float.MaxValue;
    //         int minPedestrianIndex = -1;
    //         int j = -1;
    //         foreach (var pedestrian in pedestrians) {
    //             j++;
    //             if (closestPedestrianMask[j])
    //                 continue;
    //             
    //             // var pedestrian = pedestrians[j];
    //             if(debug)
    //                 Debug.Log("Pedestrian " + pedestrian.name);
    //             if (pedestrian == this) {
    //                 closestPedestrianMask[j] = true;
    //                 continue;
    //             }
    //
    //             var pedestrianTransform = pedestrian.transform;
    //             var pedestrianPosition = pedestrianTransform.position;
    //             float distance = Vector3.Distance(position, pedestrianPosition);
    //             if (distance > viewDistance + radius) {
    //                 if(debug)
    //                     Debug.Log("Fails distance");
    //                 closestPedestrianMask[j] = true;
    //                 continue;
    //             }
    //
    //             var pedestrianDiff = pedestrianPosition - position;
    //             float pedestrianAngle = Vector3.SignedAngle(forward, pedestrianDiff, Vector3.forward);
    //             if (pedestrianAngle > SECTOR_L_END || pedestrianAngle < SECTOR_R_START ) {
    //                 if(debug)
    //                     Debug.Log("Fails angle");
    //                 closestPedestrianMask[j] = true;
    //                 continue;
    //             }
    //             
    //             if (Physics.Raycast(transform.position, pedestrianDiff, distance)) {
    //                 if (debug)
    //                     Debug.Log("Fails raycast");
    //                 closestPedestrianMask[j] = true;
    //                 continue;
    //             }
    //             
    //             if (distance < min) {
    //                 min = distance;
    //                 minPedestrianIndex = j;
    //             }
    //         }
    //         if (minPedestrianIndex >= 0) {
    //             closestPedestrians[i] = pedestrians[minPedestrianIndex];
    //             closestPedestrianMask[minPedestrianIndex] = true;
    //             closestPedestrianCount++;
    //         }
    //         else {
    //             return closestPedestrianCount;
    //         }
    //     }
    //     return visionLimit;
    // }
    
    private void FilterPedestrians() {
        var transform = this.transform;
        var forward = transform.rotation * Vector3.right;
        var position = transform.position;
        // closestPedestrianMask = new bool[pedestrians.Count];
        filteredPedestriansCount = 0;
        foreach (var pedestrian in pedestrians) {
            // var pedestrian = pedestrians[j];
            if (pedestrian == this) {
                continue;
            }

            var pedestrianTransform = pedestrian.transform;
            var pedestrianPosition = pedestrianTransform.position;
            float distance = Vector3.Distance(position, pedestrianPosition);
            if (distance > viewDistance + radius) {
                continue;
            }

            var pedestrianDiff = pedestrianPosition - position;
            float pedestrianAngle = Vector3.SignedAngle(forward, pedestrianDiff, Vector3.forward);
            if (pedestrianAngle > SECTOR_L_END || pedestrianAngle < SECTOR_R_START ) {
                continue;
            }
            
            if (Physics.Raycast(transform.position, pedestrianDiff, distance)) {
                continue;
            }

            filteredPedestrians[filteredPedestriansCount].SetPedestrian(pedestrian, distance, pedestrianAngle);
            filteredPedestriansCount++;
        }
    }


    float CalcNegativeEnergyImpact() {
        float min = float.MaxValue;
        float max = -1;
        for (int i = 0; i < obstacleImpacts.Length; i++) {
            negativeEnergies[i] = obstacleWeightFactor * obstacleImpacts[i] + (1 - obstacleWeightFactor) * collisionRisks[i];
            if (negativeEnergies[i] < min)
                min = negativeEnergies[i];
            if (negativeEnergies[i] > max)
                max = negativeEnergies[i];
        }
        // if (max > 0) {
        //     for (int i = 0; i < obstacleImpacts.Length; i++) {
        //         negativeEnergies[i] = (negativeEnergies[i] - min) / (max - min);
        //     }
        // }
        var fuzzyAttributeMap = Logic.fuzzyAttributesMap;
        inputs[fuzzyAttributeMap.GetFuzzyAttribute("NE L").index] = negativeEnergies[0];
        inputs[fuzzyAttributeMap.GetFuzzyAttribute("NE LF").index] = negativeEnergies[1];
        inputs[fuzzyAttributeMap.GetFuzzyAttribute("NE F").index] = negativeEnergies[2];
        inputs[fuzzyAttributeMap.GetFuzzyAttribute("NE RF").index] = negativeEnergies[3];
        inputs[fuzzyAttributeMap.GetFuzzyAttribute("NE R").index] = negativeEnergies[4];
        float [] outputs = Logic.ruleSetMap.ruleSets["R3"].Evaluate(inputs);
        speedNegativeEnergy = outputs[fuzzyAttributeMap.GetFuzzyAttribute("Speed").index];
        angleNegativeEnergy = outputs[fuzzyAttributeMap.GetFuzzyAttribute("Direction").index];
        
        return negativeEnergies[2];
    }

    void ScanSector(float from, float to, float sections, int index) {
        float sectorSize = to - from;
        float sectionSize = sectorSize / sections;
        var forward = transform.rotation * Vector3.right;
        float minDistance = viewDistance;
        // Obstancle detection variables
        GameObject lastObjectHit = null;
        GameObject hitObject = null;
        int numberOfHits = 0;
        float minObstacleDistance = viewDistance;
        float sectorImpact = 0;
        
        for (float angle = from; angle <= to; angle += sectionSize) {
            var direction = Quaternion.Euler(0, 0, angle) * forward;
            // Debug.DrawLine(transform.position, transform.position + direction, Color.blue);
            RaycastHit hitInfo;
            if (Physics.Raycast(transform.position, direction, out hitInfo, viewDistance)) {
                if (hitInfo.distance < minDistance)
                    minDistance = hitInfo.distance;
                hitObject = hitInfo.transform.gameObject;
                if (lastObjectHit != hitObject) {
                    if (numberOfHits != 0)
                        sectorImpact += CalculateObstacleImpact(numberOfHits, minObstacleDistance, sections);
                    numberOfHits = 1;
                    lastObjectHit = hitObject;
                    minObstacleDistance = hitInfo.distance;
                }
                else {
                    numberOfHits++;
                    if (hitInfo.distance < minObstacleDistance)
                        minObstacleDistance = hitInfo.distance;
                }
                // if (debug) {
                //     Debug.Log("Angle: " + angle + " Object: " + hitObject.name + " Distance: " + hitInfo.distance + " Hits: " + numberOfHits);
                // }
            }
        }
        if (numberOfHits > 0) {
            sectorImpact += CalculateObstacleImpact(numberOfHits, minObstacleDistance, sections);
        }
        // if (sectorImpact > 1)
        //     sectorImpact = 1;
        obstacleImpacts[index] = sectorImpact;
        // obstacleDistances[index] = Mathf.Clamp(minDistance - radius, 0, neighborDistance);
        obstacleDistances[index] = Mathf.Clamp(minDistance - radius, 0, DEFAULT_VIEW_DISTANCE);
    }

    float CalculateObstacleImpact(int numberOfHits, float minObstacleDistance, float sections) {
        float angle;
        if (numberOfHits == 1) {
            angle = 0.5f;
        }
        else {
            angle = (numberOfHits - 1);
        }
        var fuzzyAttributeMap = Logic.fuzzyAttributesMap;
        // float trueDistance = Mathf.Clamp(((minObstacleDistance - radius) / neighborDistance) * 5, 0, neighborDistance);
        float trueDistance = Mathf.Clamp(minObstacleDistance - radius, 0, DEFAULT_VIEW_DISTANCE);
        inputs[fuzzyAttributeMap.GetFuzzyAttribute("Obstacle Distance").index] = trueDistance; //minObstacleDistance - radius;
        inputs[fuzzyAttributeMap.GetFuzzyAttribute("Occupied Angle").index] = angle / sections;
        float impact = Logic.ruleSetMap.ruleSets["R1"].Evaluate(inputs)
            [fuzzyAttributeMap.GetFuzzyAttribute("Obstacle Impact").index];
        // Debug.Log(angle / sections + " " + minObstacleDistance + " " + impact);
        if (debug) {
            Debug.Log("Obstacle Impact: " + impact + " Obstacle Angle:" + angle / sections);
        }
        return impact;
    }
    
    void ShowPerceptionFull(float from, float to, float sections) {
        float diff = (to - from) / sections;
        var forward = transform.rotation * Vector3.right;
        for (float angle = from; angle <= to; angle += diff) {
            var direction = Quaternion.Euler(0, 0, angle) * forward;
            Debug.DrawLine(transform.position, transform.position + direction * viewDistance, Color.blue);
        }
    }
    
    void ShowPerception(float from, float to, float sections) {
        var forward = transform.rotation * Vector3.right;
        var direction = Quaternion.Euler(0, 0, from) * forward;
        Debug.DrawLine(transform.position, transform.position + direction * viewDistance, Color.blue);
        direction = Quaternion.Euler(0, 0, to) * forward;
        Debug.DrawLine(transform.position, transform.position + direction * viewDistance, Color.blue);
    }

    void ShowSpeedAndAngle(float speed, float angle, Color color) {
        var forward = transform.rotation * Vector3.right;
        var direction = Quaternion.Euler(0, 0, angle) * forward;
        Debug.DrawLine(transform.position, transform.position + direction * speed, color);
    }

    public void Move() {
        transform.Rotate(0f, 0f, angle * Time.fixedDeltaTime * mainController.timestep);
        var forward = transform.rotation * Vector3.right;
        float realSpeed = speed / DEFAULT_MAX_VELOCITY * maxVelocity;
        hitrost = realSpeed * mainController.timestep;
        maxHitrost = maxVelocity;
        transform.Translate(forward * (realSpeed * Time.fixedDeltaTime * mainController.timestep), Space.World);
        // GetComponent<Rigidbody>().MovePosition(forward);
    }


    // void ScanOld() {
    //     var transormRef = transform;
    //     var transormForward = transormRef.forward;
    //     var transormPosition = transormRef.position;
    //     var transormRotation = transormRef.rotation;
    //     var forward = transormRotation * Vector3.right;
    //     
    //     Debug.DrawLine(transormPosition, transormPosition + forward, Color.yellow);
    //     Debug.DrawLine(transormPosition, transormPosition + 
    //                                      Quaternion.Euler(0, 0, VIEW_ANGLE) * forward, Color.blue);
    //     Debug.DrawLine(transormPosition, transormPosition + 
    //                                      Quaternion.Euler(0, 0, -VIEW_ANGLE) * forward, Color.blue);
    //     
    //     foreach (var wall in wallColliders) {
    //         Vector3 closestPoint = wall.ClosestPointOnBounds(transform.position);
    //         Vector3 direction = closestPoint - transormPosition;
    //         float angle = Vector3.Angle(forward, direction);
    //         float distance = Vector3.Distance(closestPoint, transform.position);
    //         if (angle < VIEW_ANGLE && distance < viewDistance) {
    //             Debug.DrawLine(transform.position, closestPoint, Color.red);
    //         }
    //         else {
    //             Debug.DrawLine(transform.position, closestPoint, Color.gray);
    //         }
    //         
    //     }
    // }
}
