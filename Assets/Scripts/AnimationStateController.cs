﻿using UnityEngine;

public class AnimationStateController : MonoBehaviour
{
    private Animator animator;
    private float velocity;
    private float maxVelocity;
    private int velocityHash;
    private int maxVelocityHash;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        velocityHash = Animator.StringToHash("Velocity");
        maxVelocityHash = Animator.StringToHash("MaxVelocity");
    }

    // Update is called once per frame
    void Update() {
        velocity = PedestrianMovementController.hitrost;
        maxVelocity = PedestrianMovementController.maxHitrost;
        //  Debug.Log(velocity);
        animator.SetFloat(velocityHash, velocity);
        animator.SetFloat(maxVelocityHash, maxVelocity);
    }
}
