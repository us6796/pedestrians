﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class Spawner : MonoBehaviour {
    [SerializeField] private int nPedestrians;
    [SerializeField] private Vector2 spawnSize;
    [Range (0f, 1f)][SerializeField] private float spawnNoise = 0;
    [SerializeField] private GameObject goal;
    [SerializeField] private float startingRotation;
    [SerializeField] private float preferredVelocity;
    [SerializeField] private bool usePersonality;
    [Range (0f, 1f)][SerializeField] private float personalityPercentage = 0.25f;
    
    [Range (-100f, 100f)][SerializeField] private float minOpenness = -100;
    [Range (-100f, 100f)][SerializeField] private float maxOpenness = 100;
    
    [Range (-100f, 100f)][SerializeField] private float minConscientiousness = -100;
    [Range (-100f, 100f)][SerializeField] private float maxConscientiousness = 100;
    
    [Range (-100f, 100f)][SerializeField] private float minExtraversion = -100;
    [Range (-100f, 100f)][SerializeField] private float maxExtraversion = 100;
    
    [Range (-100f, 100f)][SerializeField] private float minAgreeableness = -100;
    [Range (-100f, 100f)][SerializeField] private float maxAgreeableness = 100;
    
    [Range (-100f, 100f)][SerializeField] private float minNeuroticism = -100;
    [Range (-100f, 100f)][SerializeField] private float maxNeuroticism = 100;
    
    public void GeneratePedestrians(GameObject pedestrian, MainController mainController) {
        if (mainController.overrideSpawners) {
            preferredVelocity = mainController.preferredVelocity;
            usePersonality = mainController.usePersonality;
            personalityPercentage = mainController.personalityPercentage;
            
            minOpenness = mainController.minOpenness;
            maxOpenness = mainController.maxOpenness;
            
            minConscientiousness = mainController.minConscientiousness;
            maxConscientiousness = mainController.maxConscientiousness;
            
            minExtraversion = mainController.minExtraversion;
            maxExtraversion = mainController.maxExtraversion;
            
            minAgreeableness = mainController.minAgreeableness;
            maxAgreeableness = mainController.maxAgreeableness;
            
            minNeuroticism = mainController.minNeuroticism;
            maxNeuroticism = mainController.maxNeuroticism;
        }
        
        float noise = spawnNoise;
        int n = nPedestrians;
        // int per = (int) (1 / personalityPercentage);
        int personalityPedestrians = (int) (nPedestrians * personalityPercentage);
        int cols = (int) Math.Ceiling(Math.Sqrt(nPedestrians));
        int rows = (int) Math.Ceiling((double) nPedestrians / cols);
        var spawnPosition = transform.position;
        float colLength = spawnSize.x / cols;
        float rowLength = spawnSize.y / rows;
        float startX = spawnPosition.x - spawnSize.x / 2;
        float startY = spawnPosition.y - spawnSize.y / 2;
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                if (n <= 0)
                    break;
                float noiseX = Random.Range(-0.4f * noise, 0.4f * noise) * colLength;
                float noiseY = Random.Range(-0.4f * noise, 0.4f * noise) * rowLength;
                float x = startX + (j * colLength + (j + 1) * colLength) / 2 + noiseX;
                float y = startY + (i * rowLength + (i + 1) * rowLength) / 2 + noiseY;
                // Debug.Log(noiseX);
                var pedestrianScript = Instantiate(pedestrian, new Vector3(x, y, 0), Quaternion.Euler(0,0,startingRotation)).GetComponent<PedestrianMovementController>();
                pedestrianScript.gameObject.name = "Subject n " + n;
                pedestrianScript.SetParameters( GetRandomColor(), goal, preferredVelocity);
                if (!usePersonality) {
                    pedestrianScript.SetPersonality(false);
                }
                mainController.pedestrians.Add(pedestrianScript);
                n--;
            }
        }

        if (usePersonality) {
            List<PedestrianMovementController> shuffled = Shuffle(mainController.pedestrians);
            foreach (var pedestrianScript in shuffled) {
                if (personalityPedestrians > 0) {
                    pedestrianScript.SetPersonality(
                        GenerateAttribute(minOpenness, maxOpenness), 
                        GenerateAttribute(minConscientiousness, maxConscientiousness),
                        GenerateAttribute(minExtraversion, maxExtraversion),
                        GenerateAttribute(minAgreeableness, maxAgreeableness),
                        GenerateAttribute(minNeuroticism, maxNeuroticism)
                    );
                }
                else {
                    pedestrianScript.SetPersonality(0,0,0,0,0);
                }
                personalityPedestrians--;
            }
        }
    }
    private static List<PedestrianMovementController> Shuffle(List<PedestrianMovementController> pedestrians) {
        return pedestrians.OrderBy(x => Random.Range(0.0f, 1.0f)).ToList();
    }

    private Color GetRandomColor() {
        return Random.ColorHSV();
    }

    private float GenerateAttribute(float minimum, float maximum) {
        if (minimum > maximum)
            return minimum;
        return Random.Range(minimum, maximum);
        // return (float) rand.NextDouble() * (maximum - minimum) + minimum;
    }
}
